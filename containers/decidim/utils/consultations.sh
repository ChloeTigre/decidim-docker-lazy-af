#!/bin/bash
sudo -u decidim bash <<EOF
source ~/.bashrc
cd /app/lazydecidim
echo "gem 'decidim-consultations'" | tee -a Gemfile
bundle
bundle exec rails decidim_consultations:install:migrations
bundle exec rails db:migrate

EOF
