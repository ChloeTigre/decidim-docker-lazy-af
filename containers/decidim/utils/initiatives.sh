#!/bin/bash
sudo -u decidim bash <<EOF
source ~/.bashrc
cd /app/lazydecidim
echo "gem 'decidim-initiatives'" | tee -a Gemfile
echo "gem 'wicked_pdf'" | tee -a Gemfile
bundle
bundle exec rails decidim_initiatives:install:migrations
bundle exec rails db:migrate
EOF
