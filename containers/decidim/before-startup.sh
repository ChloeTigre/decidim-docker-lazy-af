AVAILABLE_PLUGINS="admin blogs calendar conferences initiatives members participatory_processes sortitions votings assemblies budgets comments consultations meetings pages proposals surveys"


for i in $AVAILABLE_PLUGINS; do
    echo $DECIDIM_ENABLED_PLUGINS | grep $i && /utils/${i}.sh && echo "enabled plugin $i"
done
