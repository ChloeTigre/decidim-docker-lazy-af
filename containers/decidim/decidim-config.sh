#!/bin/bash
# generate a decidim initializer as needed.
APPLICATION_NAME=${DECIDIM_APPLICATION_NAME:-"default decidim application name"}
MAIL_SENDER=${DECIDIM_MAIL_SENDER:-"decidim@decidim.invalid"}
DEFAULT_LOCALE=${DECIDIM_DEFAULT_LOCALE:-":fr"}
AVAILABLE_LOCALES=${DECIDIM_AVAILABLE_LOCALES:-"[:fr, :en]"}
CURRENCY_UNIT=${DECIDIM_CURRENCY_UNIT:-"€"}
MAX_REPORTS_BEFORE_HIDING=${DECIDIM_MAX_REPORTS_BEFORE_HIDING:-"10"}
ENABLE_HTML_HEADER_SNIPPETS=${DECIDIM_ENABLE_HTML_HEADER_SNIPPETS:-"false"}



cat > /app/lazydecidim/config/initializers/decidim.rb <<EOF
# frozen_string_literal: true

Decidim.configure do |config|
  config.application_name = "${APPLICATION_NAME}"
  config.mailer_sender = "${MAIL_SENDER}"

  # Change these lines to set your preferred locales
  config.default_locale = ${DEFAULT_LOCALE}  # example: :en
  config.available_locales = ${AVAILABLE_LOCALES} # example: [:en, :ca, :es]

  # Geocoder configuration
  # config.geocoder = {
  #   static_map_url: "https://image.maps.cit.api.here.com/mia/1.6/mapview",
  #   here_app_id: Rails.application.secrets.geocoder[:here_app_id],
  #   here_app_code: Rails.application.secrets.geocoder[:here_app_code]
  # }

  # Custom resource reference generator method
  # config.reference_generator = lambda do |resource, component|
  #   # Implement your custom method to generate resources references
  #   "1234-#{resource.id}"
  # end

  # Currency unit
  config.currency_unit = "${CURRENCY_UNIT}"

  # The number of reports which an object can receive before hiding it
  config.max_reports_before_hiding = ${MAX_REPORTS_BEFORE_HIDING}

  # Custom HTML Header snippets
  #
  # The most common use is to integrate third-party services that require some
  # extra JavaScript or CSS. Also, you can use it to add extra meta tags to the
  # HTML. Note that this will only be rendered in public pages, not in the admin
  # section.
  #
  # Before enabling this you should ensure that any tracking that might be done
  # is in accordance with the rules and regulations that apply to your
  # environment and usage scenarios. This component also comes with the risk
  # that an organization's administrator injects malicious scripts to spy on or
  # take over user accounts.
  #
  config.enable_html_header_snippets = ${ENABLE_HTML_HEADER_SNIPPETS}
end

Rails.application.config.i18n.available_locales = Decidim.available_locales
Rails.application.config.i18n.default_locale = Decidim.default_locale


EOF
